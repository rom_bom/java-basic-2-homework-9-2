package com.company;

public class Engine {

    private int numCylinders;
    private double engineCapacity;
    private double maxPower;

    public Engine(int numCylinders, double engineCapacity, double maxPower) {
        this.numCylinders = numCylinders;
        this.engineCapacity = engineCapacity;
        this.maxPower = maxPower;
    }

    public int getNumCylinders() {
        return numCylinders;
    }

    public void setNumCylinders(int numCylinders) {
        this.numCylinders = numCylinders;
    }

    public double getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public double getMaxPower() {
        return maxPower;
    }

    public void setMaxPower(double maxPower) {
        this.maxPower = maxPower;
    }
}
